//
//  AppDelegate.swift
//  Server
//
//  Created by Alexandr on 7/14/18.
//  Copyright © 2018 Aleksandr. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        SocketManager.shared.disconnect()
    }


}

