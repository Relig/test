//
//  Message.swift
//  Server
//
//  Created by Alexandr on 7/16/18.
//  Copyright © 2018 Aleksandr. All rights reserved.
//

import Foundation

@objc class Message: NSObject {
    @objc var text: String = ""
    
    init(text: String) {
        self.text = text
    }
}
