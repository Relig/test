//
//  SocketManager.swift
//  Server
//
//  Created by Alexandr on 7/15/18.
//  Copyright © 2018 Aleksandr. All rights reserved.
//

import Foundation
import SocketIO

enum SocketConstants {
    static let port = 3000
    static let host = "http://localhost"
}

@objc class SocketManager: NSObject {
    
    static let _singletonInstance = SocketManager()

    @objc class var shared: SocketManager {
        return SocketManager._singletonInstance
    }
    
    @objc var textHandler: ((Message) -> Void)!
    @objc var connectionStatus: ((Bool) -> Void)!
    
    private lazy var manager = {
        return SocketIO.SocketManager(socketURL: URL(string: "\(SocketConstants.host):\(SocketConstants.port)")!, config: [.log(true), .compress])
    }()
    private lazy var socket = {
        return manager.defaultSocket
    }()
    
    private override init() {
        super.init()
        
        socket.on(clientEvent: .connect) { (data, ack) in
            print("socket connected")
            self.serverConnect()
            self.connectionStatus?(true)
        }
        
        socket.on(clientEvent: .disconnect) { (data, ack) in
            print("socket disconnected")
            self.connectionStatus?(false)
        }
        
        socket.on(clientEvent: .error) { (data, ack) in
            print("Error: \(data)")
            self.connectionStatus?(false)
        }
        
        socket.on("server-message") { (data, ack) in
            print("server-message received data: \(data)")
            if
                let dictionary = data.first as? [String: Any],
                let text = dictionary["text"] as? String
            {
                let message = Message(text: text)
                self.textHandler?(message)
            }
        }
        
        connect()
    }
    
    @objc func serverConnect() {
        socket.emit("server-connect", with: [])
    }
    
    @objc func serverDisconnect() {
        socket.emit("server-disconnect", with: [])
    }
    
    func connect() {
        socket.connect()
    }
    
    func disconnect() {
        socket.disconnect()
    }
    
}

