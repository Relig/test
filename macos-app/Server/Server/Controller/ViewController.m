//
//  ViewController.m
//  Server
//
//  Created by Alexandr on 7/16/18.
//  Copyright © 2018 Aleksandr. All rights reserved.
//

#import "ViewController.h"
#import "Server-Swift.h"


@interface ViewController()

@property (weak) IBOutlet NSTextField *connectionStatusLabel;
@property (unsafe_unretained) IBOutlet NSTextView *textView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    __weak ViewController *weakSelf = self;
    [[SocketManager shared] setConnectionStatus:^(BOOL value) {
        weakSelf.connectionStatusLabel.stringValue = value ? @"Connection to socket established" : @"No connection";
    }];
    [[SocketManager shared] setTextHandler:^(Message *message) {
        if ([weakSelf.textView.string isEqualToString: @""]) {
            weakSelf.textView.string = [NSString stringWithFormat:@"message: %@", message.text];
        } else {
            NSMutableString *text = [[NSMutableString alloc] initWithString: weakSelf.textView.string];
            [text appendString: [NSString stringWithFormat:@"\nmessage: %@", message.text]];
            weakSelf.textView.string = text;
        }
    }];
}


- (IBAction)segmentControlDidTap:(NSSegmentedControl*)sender {
    if (sender.selectedSegment == 0) {
        [[SocketManager shared] serverConnect];
    } else {
        [[SocketManager shared] serverDisconnect];
    }
}

@end
