//
//  ViewController.swift
//  Server
//
//  Created by Alexandr on 7/14/18.
//  Copyright © 2018 Aleksandr. All rights reserved.
//

import Cocoa

final class MainViewController: NSViewController {

    let socketManager = SocketManager.shared
    
    @IBOutlet weak var socketStatusLabel: NSTextField!
    @IBOutlet var textView: NSTextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        socketManager.connectionStatus = { [weak self] value in
            guard let `self` = self else { return }
            self.socketStatusLabel.stringValue = value ? "Connection to socket established" : "No connection"
        }
        
        socketManager.textHandler = { [weak self] value in
            guard let `self` = self else { return }
            if self.textView.string == "" {
                self.textView.string = "message: \(value.text)"
            } else {
                self.textView.string += "\nmessage: \(value.text)"
            }
        }
    }

    @IBAction func segmentControlDidChangeValue(_ sender: Any) {
        if let segmentControl = sender as? NSSegmentedControl {
            if segmentControl.selectedSegment == 0 {
                socketManager.serverConnect()
            } else {
                socketManager.serverDisconnect()
            }
        }
    }
    
}

