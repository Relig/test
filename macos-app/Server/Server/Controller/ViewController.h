//
//  ViewController.h
//  Server
//
//  Created by Alexandr on 7/16/18.
//  Copyright © 2018 Aleksandr. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface ViewController : NSViewController

- (IBAction)segmentControlDidTap:(NSSegmentedControl*)sender;


@end
