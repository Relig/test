//
//  ViewController.swift
//  Client
//
//  Created by Alexandr on 7/15/18.
//  Copyright © 2018 Aleksandr. All rights reserved.
//

import Cocoa

final class MainViewController: NSViewController {

    let socketManager = SocketManager.shared
    
    @IBOutlet weak var socketStatusLabel: NSTextField!
    @IBOutlet weak var textField: NSTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        socketManager.serverStatus = { [weak self] value in
            guard let `self` = self else { return }
            self.socketStatusLabel.stringValue = value ? "Connection established" : "No connection"
        }
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }

    @IBAction func buttonDidTap(_ sender: Any) {
        let text = textField.stringValue
        textField.stringValue = ""
        let message = Message(text: text)
        socketManager.sendMessage(message)
    }
    
}

