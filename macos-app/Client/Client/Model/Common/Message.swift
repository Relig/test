//
//  Message.swift
//  Client
//
//  Created by Alexandr on 7/16/18.
//  Copyright © 2018 Aleksandr. All rights reserved.
//

import Foundation

struct Message {
    let text: String
}
