//
//  SocketManager.swift
//  Server
//
//  Created by Alexandr on 7/15/18.
//  Copyright © 2018 Aleksandr. All rights reserved.
//

import Foundation
import SocketIO

enum SocketConstants {
    static let port = 3000
    static let host = "http://localhost"
}

final class SocketManager {
    
    static let shared = SocketManager()
    
    var serverStatus: ((Bool) -> Void)?
    
    private lazy var manager = {
        return SocketIO.SocketManager(socketURL: URL(string: "\(SocketConstants.host):\(SocketConstants.port)")!, config: [.log(true), .compress])
    }()
    private lazy var socket = {
        return manager.defaultSocket
    }()
    
    private init() {
        socket.on(clientEvent: .connect) { (data, ack) in
            print("socket connected")
        }
        
        socket.on(clientEvent: .disconnect) { (data, ack) in
            print("socket disconnected")
        }
        
        socket.on(clientEvent: .error) { (data, ack) in
            print("Error: \(data)")
        }
        
        socket.on("server-status") { (data, ack) in
            print("server-status: \(data)")
            if let serverStatus = data.first as? Bool {
                self.serverStatus?(serverStatus)
            }
        }
        
        connect()
    }
    
    func serverConnect() {
        socket.emit("server-connect", with: [])
    }
    
    func connect() {
        socket.connect()
    }
    
    func disconnect() {
        socket.disconnect()
    }
    
    func sendMessage(_ message: Message) {
        socket.emit("message", with: [["text" : message.text]])
    }
    
}

