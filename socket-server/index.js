var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var server_id = -1

function serverStatus() {
  return server_id == -1 ? false : true
}

function connectServer(socket) {
  server_id = socket.id;
  socket.broadcast.emit("server-status", serverStatus())
  console.log('Server connected');
}

function disconnectServer(socket) {
  server_id = -1
  socket.broadcast.emit("server-status", false)
  console.log('Server disconnect');
}

app.get('/', function(req, res) {
  res.send('<h1>Hello world</h1>');
});

io.on('connection', function(socket) {
  console.log('Connected ' + socket.id);

  socket.on('server-connect', function(data) {
    connectServer(socket)
  });

  socket.on('server-disconnect', function(data) {
    disconnectServer(socket)
  });

  socket.on('message', function(data) {
    if (server_id != -1) {
      io.sockets.connected[server_id].emit("server-message", data);
    }
  })

  socket.on('disconnect', function() {
    if (socket.id == server_id ) {
      disconnectServer(socket)
    } else {
      console.log('User disconnected');
    }
  });

  // send server-status
  socket.emit("server-status", serverStatus())
});

http.listen(3000, function() {
  console.log('Listening on port: 3000');
});